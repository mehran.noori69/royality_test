import React from "react";
import {CityItem} from './City'
import Vancouver from "../../.././img/cities/vancouver.jpg"
import Surrey from "../../.././img/cities/Surrey.jpg"
import Richmond from "../../.././img/cities/Richmond.jpg"
import Burnaby from "../../.././img/cities/Burnaby.jpg"
import NorthVancouver from "../../.././img/cities/NorthVancouver.jpg"
import WestVancouver from "../../.././img/cities/WestVancouver.jpg"
import Coquitlam from "../../.././img/cities/Coquitlam.jpg"
import whistler from "../../.././img/cities/whistler.jpg"
import { Container, Row } from 'reactstrap';

export class CityItemWrapper extends React.Component {

    render() {
        return (
              <Container className="city-wrapper">
                <div>A few of the beautiful cities we serve</div>
                <Row>
                    <CityItem title="Vancuver" src={Vancouver} />
                    <CityItem title="Surrey" src={Surrey} />
                    <CityItem title="Richmond" src={Richmond} />
                    <CityItem title="Burnaby" src={Burnaby} />

                    <CityItem title="NorthVancouver" src={NorthVancouver} />
                    <CityItem title="WestVancouver" src={WestVancouver} />
                    <CityItem title="Coquitlam" src={Coquitlam} />
                    <CityItem title="whistler" src={whistler} />
                </Row>
              </Container>
        )
    }
}
