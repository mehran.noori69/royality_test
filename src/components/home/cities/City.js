import React from "react";
import { Col } from 'reactstrap';
import { Link } from 'react-router-dom';

export class CityItem extends React.Component {
    render(){
        return(
          <Col lg={3}>
            <div className="city-item">
              <span>{this.props.title}</span>
              <Link to={`/residential?city=${this.props.title}`}>
                  <img src={this.props.src} alt={this.props.title}/>
              </Link>
            </div>
          </Col>
        )
    }
}
