import React from "react";
import { Col } from 'reactstrap';
import { Link } from 'react-router-dom';

export class NeighborhoodItem extends React.Component {
    render(){
        return(
          <Col lg={3}>
            <div className="neighbor-item">
              <span>{this.props.title}</span>
              <Link to={`/residential?neighborhood=${this.props.id}&`}>
                <img src={this.props.src} alt={this.props.title}/>
              </Link>
            </div>
          </Col>
        )
    }
}
