import React from "react";
import {NeighborhoodItem} from './Neighborhood'

import Downtown from "../../.././img/neighborhoods/Downtown.jpg"
import Shaughnessy from "../../.././img/neighborhoods/Shaughnessy.jpg"
import CoalHarbour from "../../.././img/neighborhoods/CoalHarbour.jpg"
import Yaletown from "../../.././img/neighborhoods/Yaletown.jpg"

import WestEnd from "../../.././img/neighborhoods/WestEnd.jpg"
import Kitsilano from "../../.././img/neighborhoods/Kitsilano.jpg"
import FalseCreek from "../../.././img/neighborhoods/FalseCreek.jpg"
import MountPleasant from "../../.././img/neighborhoods/MountPleasant.jpg"

import PointGrey from "../../.././img/neighborhoods/PointGrey.jpg"
import EdgemontVillage from "../../.././img/neighborhoods/EdgemontVillage.jpg"
import UpperLonsdale from "../../.././img/neighborhoods/UpperLonsdale.jpg"
import LowerLonsdale from "../../.././img/neighborhoods/LowerLonsdale.jpg"

import DeepCove from "../../.././img/neighborhoods/DeepCove.jpg"
import BritishProperties from "../../.././img/neighborhoods/BritishProperties.jpg"
import Guildford from "../../.././img/neighborhoods/Guildford.jpg"
import WhiteRock from "../../.././img/neighborhoods/WhiteRock.jpg"

import { Container, Row } from 'reactstrap';

export class NeighborhoodWrapper extends React.Component {

    render() {
        return (
              <Container className="neighbor-wrapper">
                <div>A few of the neighborhoods we love to serve</div>
                <Row>
                    <NeighborhoodItem title="Downtown" src={Downtown} id="1246"/>
                    <NeighborhoodItem title="Shaughnessy" src={Shaughnessy} id="911"/>
                    <NeighborhoodItem title="CoalHarbour" src={CoalHarbour} id="1047"/>
                    <NeighborhoodItem title="Yaletown" src={Yaletown} id="897"/>

                    <NeighborhoodItem title="WestEnd" src={WestEnd} id="1023"/>
                    <NeighborhoodItem title="Kitsilano" src={Kitsilano} id="1033"/>
                    <NeighborhoodItem title="FalseCreek" src={FalseCreek} id="1076"/>
                    <NeighborhoodItem title="MountPleasant" src={MountPleasant} id="1167"/>

                    <NeighborhoodItem title="PointGrey" src={PointGrey} id="981"/>
                    <NeighborhoodItem title="EdgemontVillage" src={EdgemontVillage} id="1060"/>
                    <NeighborhoodItem title="UpperLonsdale" src={UpperLonsdale} id="1125"/>
                    <NeighborhoodItem title="LowerLonsdale" src={LowerLonsdale} id="1002"/>

                    <NeighborhoodItem title="DeepCove" src={DeepCove} id="1044"/>
                    <NeighborhoodItem title="BritishProperties" src={BritishProperties} id="1001"/>
                    <NeighborhoodItem title="Guildford" src={Guildford} id="1025"/>
                    <NeighborhoodItem title="WhiteRock" src={WhiteRock} id="1185"/>
                </Row>
              </Container>
        )
    }
}
