import React from "react";
import { Container } from 'reactstrap';
// Should export connected component without {}
import Antselect from './Antselect';
import { connect } from "react-redux";
import { fetchCityItem } from "../.././redux/CityActions";
import { fetchNeighborSearchItem } from "../.././redux/NeighborActions";
import { withRouter } from "react-router-dom";

class SearchBox extends React.Component {

  componentWillMount(){
    this.props.dispatch(fetchCityItem());
    this.props.dispatch(fetchNeighborSearchItem());
  }

    render(){
        return(
          <Container className="search-box">
            <div className="search-row">Serving the Greater Vancouver with an unparalleled Real Estate Search Engine</div>
            <br/>
            {/* <div className="search-row"> */}
              <Antselect/>
            {/* </div> */}
            <div className="search-row">Your needs are unique, so should your REALTOR's ® expertise Let us match you with the right REALTOR ®</div>
          </Container>
        )
    }
}

// You should export this and not export Antselect
export default withRouter(connect(null)(SearchBox));
