import React from "react";
import { Select } from 'antd';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import 'antd/dist/antd.css'
import { FaSearch } from 'react-icons/fa';
import { Link } from 'react-router-dom';


class Antselect extends React.Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      city: "",
      neighborhood: ""
    };
  }

  handleChange(value) {
    var cit = ""
    var neigh = ""
    for(var i=0; i< value.length; i++){
      if (value[i].includes("city")){
        cit += value[i].split("=")[1]+","
      }
      if (value[i].includes("neigh")){
        neigh += value[i].split("=")[1]+","
      }
      this.setState ({
          city: cit,
          neighborhood: neigh
      });
    }
  }

  render() {
    const { city, neighborhood } = this.state;
    const { Option, OptGroup } = Select;
    const opt1 =
              <OptGroup label="city" key="city">
                {this.props.cities.map((city, i) => (
                  <Option key={"city"+i} value={"city="+city.name+"_"+city.state}>{city.name}</Option>
                ))}
              </OptGroup>
    const opt2 =
              <OptGroup label="neighborhood" key="neighborhood">
                {this.props.neighbors.map((neighbor, i) => (
                  <Option key={"neighbor"+i} value={"neigh="+neighbor.id+"="+neighbor.name+"("+neighbor.city+")"}>{neighbor.name+" ("+neighbor.city+")"}</Option>
                ))}
              </OptGroup>

    return (
      <div className="search-row">
        <Select
          mode="multiple"
          maxTagCount={2}
          placeholder="Search by City, neighborhood or MLS number..."
          defaultValue={[]}
          style={{ width: 400 }}
          onChange={this.handleChange}>
          {opt1}{opt2}
        </Select>
        <Link to={`/residential?city=${city}&neighborhood=${neighborhood}`}>
          <i className="search_btn"><FaSearch size={20}/></i>
        </Link>
      </div>
    );
  }

}

function mapStateToProps(state) {
  // state is equal to store.getState()
  return {
    cities: state.cityItems.items,
    neighbors: state.neighborhoodItems.items,
  }
};

// You should export this and not export Antselect
export default withRouter(connect(mapStateToProps)(Antselect));
