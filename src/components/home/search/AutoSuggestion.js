import React from "react";
import Autosuggest from 'react-autosuggest';
import '../../.././autosuggest.css';
import { connect } from "react-redux";
import { fetchSearchItem } from "../.././redux/SearchItemActions";
import { withRouter } from "react-router-dom";

class AppAutosuggest extends React.Component {

  constructor() {
    super();
    this.state = {
      value: '',
      suggestions: []
    };
  }

  escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }

  getSuggestions(value) {
    const escapedValue = this.escapeRegexCharacters(value.trim());

    if (escapedValue === '') {
      return [];
    }

    const items = this.props.items;
    const regex = new RegExp('^' + escapedValue, 'i');

    return items
      .map(section => {
        return {
          title: section.title,
          languages: section.languages.filter(language => regex.test(language.name))
        };
      })
      .filter(section => section.languages.length > 0);
  }

  getSuggestionValue(suggestion) {
    return suggestion.name;
  }

  renderSuggestion(suggestion) {
    return (
      <span>{suggestion.name}</span>
    );
  }

  renderSectionTitle(section) {
    return (
      <strong>{section.title}</strong>
    );
  }

  getSectionSuggestions(section) {
    return section.languages;
  }

  onChange = (event, { newValue, method }) => {
    this.setState({
      value: newValue
    });
  };

  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getSuggestions(value)
    });
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  componentDidMount() {
    this.props.dispatch(fetchSearchItem());
  }

  render() {
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: "Search by City, neighborhood or MLS number...",
      value,
      onChange: this.onChange
    };

    return (
      <Autosuggest
        multiSection={true}
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        getSuggestionValue={this.getSuggestionValue}
        renderSuggestion={this.renderSuggestion}
        renderSectionTitle={this.renderSectionTitle}
        getSectionSuggestions={this.getSectionSuggestions}
        inputProps={inputProps} />
    );
  }
}

function mapStateToProps(state) {
  // state is equal to store.getState()
  console.log(state.searchItems.items);
  return {
    items: state.searchItems.items,
  }
};

// You should export this and not export AppAutosuggest
export default withRouter(connect(mapStateToProps)(AppAutosuggest));
