import {
  Fetch_SearchItems_Begin,
  Fetch_SearchItems_Success,
  Fetch_SearchItems_Failure
} from './CityActions.js';

const initialState = {
  items: [],
  loading: false,
  error: null
};

export default function CityReducer(state = initialState, action) {
  switch(action.type) {
    case Fetch_SearchItems_Begin:
      return {
        ...state,
        loading: true,
        error: null
      };
    case Fetch_SearchItems_Success:
      return {
        ...state,
        loading: false,
        items: action.payload.searchItems,
      };
    case Fetch_SearchItems_Failure:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        items: []
      };
    default:
      return state;
  }
}
