import {
  Fetch_NeighborSearchItems_Begin,
  Fetch_NeighborSearchItems_Success,
  Fetch_NeighborSearchItems_Failure
} from './NeighborActions.js';

const initialState = {
  items: [],
  loading: false,
  error: null
};

export default function NeighborhoodReducer(state = initialState, action) {
  switch(action.type) {
    case Fetch_NeighborSearchItems_Begin:
      return {
        ...state,
        loading: true,
        error: null
      };
    case Fetch_NeighborSearchItems_Success:
      return {
        ...state,
        loading: false,
        items: action.payload.searchItems,
      };
    case Fetch_NeighborSearchItems_Failure:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        items: []
      };
    default:
      return state;
  }
}
