import { combineReducers } from "redux";
import cityItems from "./CityReducer";
import neighborhoodItems from "./NeighborhoodReducer";

export default combineReducers({
  cityItems,
  neighborhoodItems
});
