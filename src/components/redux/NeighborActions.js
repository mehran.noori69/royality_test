export const Fetch_NeighborSearchItems_Begin   = 'Fetch_NeighborSearchItems_Begin';
export const Fetch_NeighborSearchItems_Success = 'Fetch_NeighborSearchItems_Success';
export const Fetch_NeighborSearchItems_Failure = 'Fetch_NeighborSearchItems_Failure';

export const fetchNeighborSearchItemsBegin = () => ({
  type: Fetch_NeighborSearchItems_Begin
});

export const fetchNeighborSearchItemsSuccess = searchItems => ({
  type: Fetch_NeighborSearchItems_Success,
  payload: { searchItems }
});

export const fetchNeighborSearchItemsFailure = error => ({
  type: Fetch_NeighborSearchItems_Failure,
  payload: { error }
});

export function fetchNeighborSearchItem() {
  return dispatch => {
    // dispatch(fetchSearchItemsBegin());
    return fetch("https://gooshichand.com/android/v1/neighborhood/", {method: 'GET'})
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchNeighborSearchItemsSuccess(json));
        return json;
      })
      .catch(error => dispatch(fetchNeighborSearchItemsFailure(error)));
  };
}

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
