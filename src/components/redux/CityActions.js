export const Fetch_SearchItems_Begin   = 'Fetch_SearchItems_Begin';
export const Fetch_SearchItems_Success = 'Fetch_SearchItems_Success';
export const Fetch_SearchItems_Failure = 'Fetch_SearchItems_Failure';

export const fetchSearchItemsBegin = () => ({
  type: Fetch_SearchItems_Begin
});

export const fetchSearchItemsSuccess = searchItems => ({
  type: Fetch_SearchItems_Success,
  payload: { searchItems }
});

export const fetchSearchItemsFailure = error => ({
  type: Fetch_SearchItems_Failure,
  payload: { error }
});

export function fetchCityItem() {
  return dispatch => {
    // dispatch(fetchSearchItemsBegin());
    return fetch("https://gooshichand.com/android/v1/city/", {method: 'GET'})
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchSearchItemsSuccess(json));
        return json;
      })
      .catch(error => dispatch(fetchSearchItemsFailure(error)));
  };
}

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
