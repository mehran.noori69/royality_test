import React from 'react';
import {CityItemWrapper} from './components/home/cities/CityItemWrapper'
import {NeighborhoodWrapper} from './components/home/neighborhood/NeighborhoodWrapper'
import SearchBox from './components/home/search/SearchBox'

class Home extends React.Component{
    render() {
      return (
          <div className="home_bg_image">
            <header></header>

            <main id="mainContainer">
                <div id="search-box"><SearchBox/></div>
                <div id="city-wrapper"><CityItemWrapper/></div>
                <div id="neighbor-wrapper"><NeighborhoodWrapper/></div>
            </main>

            <footer id="footer" className="revealed"></footer>
          </div>
      );
  }

}
export default Home;
