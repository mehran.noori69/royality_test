import React from "react";
import ReactDOM from "react-dom";
import {Route, BrowserRouter, Switch} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import Home from "./Home";
import ScrollToTop from "./ScrollToTop";

import rootReducer from './components/redux/RootReducer';
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { Provider } from 'react-redux'

const store = createStore(
  rootReducer,
  applyMiddleware(thunk)
);

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
              <ScrollToTop>
                <Switch>
                    <Route exact path="/" component={Home}/>
                </Switch>
              </ScrollToTop>
        </BrowserRouter>,
    </Provider>,
  document.getElementById("root")
);
